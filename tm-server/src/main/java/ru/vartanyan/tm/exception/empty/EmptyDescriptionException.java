package ru.vartanyan.tm.exception.empty;

public class EmptyDescriptionException extends Exception {

    public EmptyDescriptionException() {
        super("Error! Description cannot be null or empty...");
    }

}
