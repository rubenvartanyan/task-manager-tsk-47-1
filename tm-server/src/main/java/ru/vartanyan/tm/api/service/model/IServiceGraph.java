package ru.vartanyan.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.AbstractEntity;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractEntityGraph;

import java.util.List;

public interface IServiceGraph<E extends AbstractEntityGraph> {

    void add(@NotNull E entity) throws NullObjectException;

    void addAll(@NotNull List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    E findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}

