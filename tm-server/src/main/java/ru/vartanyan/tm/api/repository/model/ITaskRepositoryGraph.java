package ru.vartanyan.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.model.TaskGraph;

import java.util.List;

public interface ITaskRepositoryGraph extends IRepositoryGraph<TaskGraph> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<TaskGraph> findAll();

    @NotNull
    List<TaskGraph> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<TaskGraph> findAllByUserId(@Nullable String userId);

    @NotNull
    TaskGraph findOneById(@Nullable String id);

    @NotNull
    TaskGraph findOneByIdAndUserId(
            @Nullable String userId,
            @NotNull String id
    );

    @NotNull
    TaskGraph findOneByIndex(
            @Nullable String userId,
            @NotNull Integer index
    );

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void unbindTaskFromProjectId(@NotNull String userId,
                                 @NotNull String id);

    @NotNull
    TaskGraph findOneByName(
            @Nullable String userId,
            @NotNull String name
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId,
                                @NotNull String id);

    void removeOneByName(
            @Nullable String userId,
            @NotNull String name
    );

}
