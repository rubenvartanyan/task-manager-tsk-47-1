package ru.vartanyan.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractCommand;

import java.util.jar.Manifest;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public @NotNull String name() {
        return "show-about";
    }

    @Override
    public String description() {
        return "Show about developer info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("developerEmail"));
    }

}
